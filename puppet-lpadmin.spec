Name:           puppet-lpadmin
Version:        2.1
Release:        1%{?dist}
Summary:        Masterless puppet module for lpadmin

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch
Requires:       puppet-agent

%description
Masterless puppet module for lpadmin.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/lpadmin/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/lpadmin/
touch %{buildroot}/%{_datadir}/puppet/modules/lpadmin/linuxsupport

%files -n puppet-lpadmin
%{_datadir}/puppet/modules/lpadmin
%doc code/README.md

%post
MODULE=$(echo %{name} | cut -d \- -f2)
if [ -f %{_datadir}/puppet/modules/${MODULE}/linuxsupport ]; then
  grep -qE "autoreconfigure *= *True" /etc/locmap/locmap.conf && AUTORECONFIGURE=1 || :
  if [ $AUTORECONFIGURE ]; then
    locmap --list |grep $MODULE |grep -q enabled && MODULE_ENABLED=1 || :
    if [ $MODULE_ENABLED ]; then
      echo "locmap autoreconfigure enabled, running: locmap --configure $MODULE"
      locmap --configure $MODULE || :
    else
      echo "locmap autoreconfigure enabled, however the $MODULE module is not enabled"
      echo "Skipping (re)configuration of $MODULE"
    fi
  fi
fi

%changelog
* Tue Oct 08 2024 Ben Morrice <ben.morrice@cern.ch> 2.1-1
- Add autoreconfigure %post script

* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.0-4
- Bump release for disttag change

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.0-3
- fix requires for puppet-agent

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-2
- rebuild for el8

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebuild for 7.5 release

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Wed Oct 19 2016 Aris Boutselis <aris.boutselis@cern.ch>
-Initial release
