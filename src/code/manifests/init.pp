# == Class: lpadmin
class lpadmin (
  $buildings = [],
  $force = false,
  ) {
  package{ 'lpadmincern':
    ensure => present,
  }
  if !$facts['is_virtual'] or $force {
    $buildings.each|Integer $building| {
      exec{"Install printers in building ${building}":
        command => "/usr/sbin/lpadmincern --add --building ${building} ",
        require => Package['lpadmincern'],
        timeout => 20,
      }
    }
  }
}
